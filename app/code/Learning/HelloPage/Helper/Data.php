<?php
namespace Learning\HelloPage\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{

    const XML_PATH_CRON_TEXT  	= 'hellopage_config/test/custom_text';
    protected $scopeConfigInterface;

    public function __construct(
        Context $context
    ) {
        $this->scopeConfigInterface = $context->getScopeConfig();
        parent::__construct($context);
    }

    /**
     * Return true if module is enabled on frontend
     * @return bool
     */
    public function getCustomText()
    {
        return (string) $this->scopeConfigInterface->getValue(self::XML_PATH_CRON_TEXT);
    }
}
?>
