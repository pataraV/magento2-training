<?php
namespace Learning\HelloPage\Api;

interface PostManagementInterface {

    /**
     * GET for Post api
     * @param string $name
     * @return string
     */

    public function getPost($name);
}
