<?php

namespace Learning\HelloPage\Cron;

class FirstCron
{
    protected $_helper;
    public function __construct(\Learning\HelloPage\Helper\Data $helper)
    {
        $this->_helper  = $helper;
    }

    public function execute()
    {
        $configValue = $this->_helper->getCustomText();
        $cronTextFromConfig = 'Cron Run: FirstCron : '.$configValue;
        $logger = \Magento\Framework\App\ObjectManager::getInstance()->create('Psr\Log\LoggerInterface');
        $logger->addDebug(var_export($cronTextFromConfig, true));

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($cronTextFromConfig);

        return $this;
    }
}
