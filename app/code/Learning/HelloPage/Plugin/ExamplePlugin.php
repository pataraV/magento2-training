<?php
namespace Learning\HelloPage\Plugin;

class ExamplePlugin
{

    public function beforeSetTitle(\Learning\HelloPage\Controller\Page\Plugin $subject, $title)
    {
        $title = $title . " to ";
        echo __METHOD__ . "</br>";

        return [$title];
    }

    public function afterGetTitle(\Learning\HelloPage\Controller\Page\Plugin $subject, $result)
    {

        echo __METHOD__ . "</br>";

        return '<h1>'. $result . 'Mageplaza.com' .'</h1>';

    }


    public function aroundGetTitle(\Learning\HelloPage\Controller\Page\Plugin $subject, callable $proceed)
    {

        echo __METHOD__ . " - Before proceed() </br>";
        $result = $proceed();
        echo __METHOD__ . " - After proceed() </br>";


        return $result;
    }

}
