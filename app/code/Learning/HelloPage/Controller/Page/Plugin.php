<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Learning\HelloPage\Controller\Page;
class Plugin extends \Magento\Framework\App\Action\Action
{
    protected $title;

    public function execute()
    {
        echo $this->setTitle('Welcome.');
        echo $this->getTitle();
    }

    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function getTitle()
    {
        echo 'call getTitle';
        return $this->title;
    }
}
