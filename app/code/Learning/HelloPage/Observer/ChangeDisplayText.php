<?php

namespace Learning\HelloPage\Observer;

class ChangeDisplayText implements \Magento\Framework\Event\ObserverInterface
{
    protected $_helper;
    public function __construct(\Learning\HelloPage\Helper\Data $helper)
    {
        $this->_helper  = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $configValue = $this->_helper->getCustomText();
        $displayText = $observer->getData('acom_text');
//        echo $displayText . " - Event </br>";

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($displayText.':'.$configValue);

        return $this;
    }
}
